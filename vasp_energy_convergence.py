#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from oszicar import oszicar


osz = oszicar().read('vasp.out')
total_energies = osz.get_energies()

i = 0
while True:
    try:
        tot_ediff = total_energies[int(2**i)] - total_energies[0]#- total_energies[int(2**(i-1))]
        prev_ediff = total_energies[int(2**i)] - total_energies[int(2**(i-1))]
        print '%6i %10.4f %10.4f' % (int(2**i), tot_ediff, prev_ediff)
        i += 1
    except IndexError:
        break
print '%6i %10.4f' % (len(total_energies), total_energies[-1] - total_energies[0])
