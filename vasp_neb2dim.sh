#!/bin/bash +x

if [ "$#" -ne 1 ]; then
    echo 'usage:' "$0" '<neb directory>'
    exit -1
fi

if [ ! -d $1 ]; then
    echo $1 does not exist
    exit -1
fi

cp -r $1 neb_backup
cd $1
nebresults.pl
gunzip */OUTCAR.gz
vfin.pl fin
neb2dim.pl
mv dim ../neb2dim
cd ..
rm -rf $1
cp -r neb_backup $1
rm -rf neb_backup
