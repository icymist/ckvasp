#! /usr/bin/env python

import subprocess as sp
import numpy as np
import ase.io
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description='')
    
    parser.add_argument('-t', '--reqtemp', type=float, required=True)
    
    args = parser.parse_args()
    req_temp = args.reqtemp

    temp = sp.check_output("F | awk '{print $3}'", shell=True)

    temp = [float(i) for i in temp.strip().split()]
    temp = np.array(temp)

    natoms = len(ase.io.read('POSCAR'))

    print '%30s %i' % ('No. of atoms:', natoms)
    print '%30s %.2f' % ('Required temperature:', req_temp)
    print '%30s %.2f' % ('Required sqrt. variance:', np.sqrt(2*req_temp**2/(3*natoms)))

    print '%30s %.2f' % ('Mean temperature:', np.mean(temp))
    print '%30s %.2f' % ('Sqrt. variance:', np.sqrt(np.var(temp)))
