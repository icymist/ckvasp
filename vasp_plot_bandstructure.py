#!/usr/bin/env python

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import eigenval as er

if (__name__ == '__main__'):
    filename = 'EIGENVAL'
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        print("Defaulting filename to 'EIGENVAL', if you wish to specify another filename on the command prompt do it like this: plot_eigenval.py myfilename")

    parser = er.EigenvalParser()
    kpoints = parser.parse(filename)
    #print kpoints

    #rcParams['lines.linewidth'] = 4.0
    #rcParams['font.weight'] = "heavy"
    #rcParams['axes.linewidth'] = 4.0
    #rcParams['xtick.major.size'] = 0.0
    #rcParams['xtick.minor.size'] = 0.0
    #rcParams['ytick.major.size'] = 0.0
    #rcParams['ytick.minor.size'] = 0.0  

    ##efermi = 6.4081
    #efermi = 0

    for i, band in enumerate(er.get_bands(kpoints)):
        print i, band
        print
        #plot(range(0,len(band)), array(band)-efermi, 'k')

    #xlim(0,119)
    #ylim(-25,2)
    #plt.title("Band Structure")
    #xlabel("K-Points")
    #ylabel("Energy(eV)")
    #plt.show()
    #plt.savefig('bandstructure.png')
