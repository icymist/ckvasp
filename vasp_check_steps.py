#!/usr/bin/env python
# encoding: utf-8

import argparse
import yaml


def check_steps(steps):
    start_in_directories = [step['start_in'] for step in steps]
    start_from_directories = [step['start_from'] for step in steps]

    # ensure that all the start_in_directories are unique
    if len(set(start_in_directories)) != len(start_in_directories):
        repeated_directories = [d for d in start_in_directories if
                                start_in_directories.count(d) > 1]
        repeated_directories = set(repeated_directories)
        print "Overwrite:"
        print repeated_directories

    """
    Ensure that all the start_from_directories exist as start_in_directories
    except the first one.
    """
    no_previous = []
    for directory in start_from_directories[1:]:
        if directory not in start_in_directories:
            no_previous.append(directory)

    if no_previous:
        print "No previous:"
        for directory in no_previous:
            print directory

    return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Script to check sanity of steps.yaml")
    parser.add_argument('-i', '--input-file', default='steps.yaml')
    args = parser.parse_args()

    steps = yaml.load(open(args.input_file))
    check_steps(steps)
