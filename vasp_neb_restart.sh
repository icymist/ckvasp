#! /bin/bash

dirs=`ls -d ??`

for dir in $dirs; do
    cd $dir
    pwd
    cp -i POSCAR POSCAR.old
    mv CONTCAR POSCAR
    cd ..
done
