#!/usr/bin/env python
# encoding: utf-8

def test_make_potcar():
    import make_potcar
    assert make_potcar.get_potcar(['Si', 'O'], 'pbe_new') == open('tests/data/POTCAR.SiO.PBENEW').read()
    assert make_potcar.get_potcar(['Si', 'O'], 'pbe') == open('tests/data/POTCAR.SiO.PBE').read()
    assert make_potcar.get_potcar(['Si', 'O'], 'lda_new') == open('tests/data/POTCAR.SiO.LDANEW').read()
