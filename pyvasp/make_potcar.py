#! /usr/bin/env python

import os, sys
import subprocess as sp
import argparse
from poscar import poscar
import logging

logging.basicConfig(level=logging.DEBUG)

try:
    vasppp = os.environ['VASP_PP_PATH']
except KeyError:
    logging.critical('Please set the VASP_PP_PATH environment variable')

def get_potcar(elements, xc):
    if xc.lower() == 'pbe':
        ppdir = os.path.join(vasppp, 'pot_PAW_PBE')
    elif xc.lower() == 'pbe_new':
        ppdir = os.path.join(vasppp, 'pot_PAW_PBENEW')
    elif xc.lower() == 'lda_new':
        ppdir = os.path.join(vasppp, 'pot_PAW_LDANEW')

    pp = ''
    for e in elements:
        logging.info('Copying POTCAR from: {}'.format(os.path.join(ppdir, e, 'POTCAR')))
        pp += open(os.path.join(ppdir, e, 'POTCAR')).read()

    return pp

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-e', '--elements', nargs='*', default=[])
    parser.add_argument('-f', '--poscar', default='POSCAR')
    parser.add_argument('-xc', '--xc', choices=['pbe', 'pbe_new', 'lda_new'], default='pbe_new')
    parser.add_argument('-o', '--out_potcar', default='POTCAR')

    args = parser.parse_args()

    if args.elements:
        elements = args.elements
    elif args.poscar:
        pos = poscar().read(args.poscar)
        version = pos.version
        logging.info('POTCAR version {}'.format(version))
        if version == '5':
            nline = 5
        elif version == '4':
            nline = 0
        elements = open(args.poscar).readlines()[nline].strip().split()
        elements_str = ' '.join('{}'.format(ele) for ele in elements)
        logging.info('Elements: {}'.format(elements_str))
    else:
        logging.critical('Elements not given on command line and cannot get elements from POSCAR')
        sys.exit(-1)

    if elements:
        pp = get_potcar(elements, args.xc)
        p = open(args.out_potcar, 'w')
        p.write(pp)
        p.close()
    else:
        logging.critical('No elements')
        sys.exit(-1)
