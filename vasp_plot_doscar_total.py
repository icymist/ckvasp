#! /usr/bin/env python

import numpy as np
from cStringIO import StringIO as sio
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

"""Plots the total DoS from the DOSCAR file from VASP"""

f = open('DOSCAR')
l = f.readlines()

natoms = int(l[0].strip().split()[1])
hold = l[5].strip().split()
ndat, efermi = int(hold[2]), float(hold[3])
#print ndat, efermi

dat = sio(''.join(l[6:6+ndat]).strip())
energy, dos, intdos = np.loadtxt(dat, unpack=True)

shifted_energy = energy - efermi
dos_per_atom = dos/natoms

#for ienergy, idos, iintdos in zip(energy, dos, intdos):
#    print energy, dos, intdos

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(shifted_energy, dos_per_atom)
xlabel = 'E - Efermi [%.2f eV]' % (efermi)
ax.set_xlabel(xlabel)
ax.set_ylabel('DoS/atom')
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,y2))

plt.savefig('dos.png')
