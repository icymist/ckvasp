#!/usr/bin/env python
# encoding: utf-8

"""
Calculates the number of different atoms to be placed in a simulation box
of given size.

    input:
        - density in g/cm^3
        - elements
        - mole fractions

    output:
        - total number of atoms in simulation box
        - number of atoms of each element
"""

import ase
import argparse
import numpy as np

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Script to calculated the required box size given the \
        constitution and density')

    parser.add_argument('-d', '--density',
                        type=float,
                        help='density in g/cm^-3')
    parser.add_argument('-e', '--elements',
                        nargs='*',
                        help='list of elements')
    parser.add_argument('-x', '--mole_fractions',
                        type=float,
                        nargs='*',
                        help='mole fractions')
    parser.add_argument('-s', '--side_of_box',
                        type=float,
                        default=12.0,
                        help='Side of cubic box')

    args = parser.parse_args()
    density = args.density
    elements = args.elements
    mole_fractions = args.mole_fractions
    assert np.isclose(np.sum(mole_fractions), 1.0)
    side_of_box = args.side_of_box

    density = density * 1e-24  # g/cm^-3 to g/Ang^-3

    mol_weights = [ase.Atom(e).mass for e in elements]
    Na = 6.022e23  # Avogadro number

    numerator = density * side_of_box**3
    denominator = sum([x*m for x, m in zip(mole_fractions, mol_weights)])
    denominator = denominator/Na

    total_number_of_atoms = numerator/denominator
    total_number_of_atoms = int(total_number_of_atoms + 0.5)

    print 'Total number of atoms: {:3d}'.format(total_number_of_atoms)
    for e, x in zip(elements, mole_fractions):
        number_of_atoms = int(round(x*total_number_of_atoms))
        print '{:2s}: {:3d}'.format(e, number_of_atoms)
